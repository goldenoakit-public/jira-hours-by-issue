import unittest
from jira import JIRA
import jira
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../modules")
import ticket_exporter
from unittest.mock import MagicMock


#   get_issues should take timeframe and return all issues in this time


#  get_hours_by_issue should take timeframe and options( add_comment, add_labels)
# it should get_issues

class TestTicketExporter(unittest.TestCase):
    def setUp(self):
        self.jira = JIRA(os.environ['JIRA_URL'],auth=(os.environ['JIRA_USERNAME'], os.environ['JIRA_PASSWORD']))

        self.te = ticket_exporter.TicketExporter(self.jira)
        self.timeframe = 'worklogDate >= startOfMonth(-1) AND worklogDate <= endOfMonth(-1)'

    def test_getIssues(self):
        self.jira.search_issues = MagicMock
        self.te.getIssues(self.timeframe)

        assert self.jira.search_issues.called
    
    def test_calculateTotalTimeInHours(self):
        timeInSeconds = 7200
        expected = 2.0
        actual = self.te.calculateTotalTimeInHours(timeInSeconds)
        self.assertEqual(expected, actual)

    def test_printHoursByIssue (self):
        self.jira.search_issues = MagicMock(return_value=[])
        self.te.getIssues = MagicMock
        self.te.thisMonth = MagicMock
        onlyThisMonth = True
        self.te.printHoursByIssue(self.timeframe,onlyThisMonth)
        assert self.te.getIssues.called
        assert self.te.thisMonth.called

if __name__ == '__main__':
    unittest.main()