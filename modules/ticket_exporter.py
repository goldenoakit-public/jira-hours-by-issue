from jira import JIRA
import os

import pickle
from datetime import datetime

class TicketExporter:

    def __init__(self, jira): 
        self.BIG_NUMBER=  99999 # inf doesn't work
        self.jira = jira
        self.SECONDS_PER_HOURS = 1.0/3600
        self.today = datetime.today()

    def getIssues(self, timeframe):
        return self.jira.search_issues( "(worklogAuthor = "+os.environ['WorkLogAuthor']+" AND "+timeframe+" )", maxResults=self.BIG_NUMBER )
    
    def calculateTotalTimeInHours(self,timeInSeconds):
        return round(timeInSeconds*self.SECONDS_PER_HOURS,2)

    def thisMonth(self, dateCreated):
        yearAndMonth = str(self.today.year)+"-"+str(self.today.month)+"-"
        if dateCreated.find(yearAndMonth)!=-1:
            return True
        else:
            return False

    def printHoursByIssue(self,timeframe,onlyThisMonth):
        totalTimeSpendInSeconds = 0
        for issue_ref in self.getIssues(timeframe):
            try:
                issue = self.jira.issue(issue_ref.key)
                print('{} - {} '.format( issue.key, issue.fields.summary))
                totalIssueTime = 0
                for iterator in range(len(issue.fields.worklog.worklogs)) :
                    try:    
                        if onlyThisMonth and not self.thisMonth(issue.fields.worklog.worklogs[iterator].created):
                            continue
                        print( "   -",issue.key, issue.fields.worklog.worklogs[iterator].comment, issue.fields.worklog.worklogs[iterator].timeSpent,', created: ', issue.fields.worklog.worklogs[iterator].created)
                        totalIssueTime = totalIssueTime +  issue.fields.worklog.worklogs[iterator].timeSpentSeconds
                    except Exception as e:
                        print(e)
                        continue
                print('Total time spend this month : {} and in total {}\n'.format(self.calculateTotalTimeInHours(totalIssueTime),  issue.fields.timetracking.timeSpent))
                totalTimeSpendInSeconds = totalTimeSpendInSeconds + totalIssueTime
            except Exception as e:
                print("Problem with getting timeSpend for issue:", issue_ref.key, "\n")
                print(e)
        print("Total time spend: ", self.calculateTotalTimeInHours(totalTimeSpendInSeconds)," hours")
