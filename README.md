# jira-hours-by-issue

Tool to get hours spend by issue in a given month by a user.
## starting ##

```
export JIRA_URL="https://jira.example.com"
export JIRA_USERNAME="your_username"
export JIRA_PASSWORD="Password"
export WorkLogAuthor="worklog_author"

python3.7 main.py
```
