# run with python 3.7
import modules.ticket_exporter as ticket_exporter
import modules.spreadsheet_generator as spreadsheet_generator
from jira import JIRA
import os

if __name__ == '__main__':
    timeframe  = 'worklogDate >= startOfMonth() AND worklogDate <= endOfMonth()'
    onlyThisMonth = True
    jira = JIRA(os.environ['JIRA_URL'],auth=(os.environ['JIRA_USERNAME'], os.environ['JIRA_PASSWORD']))
    ticket_exporter = ticket_exporter.TicketExporter(jira)
    ticket_exporter.printHoursByIssue(timeframe,onlyThisMonth)
